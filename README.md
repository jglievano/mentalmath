MentalMath
==========

A game for improving your mental math skills!

## Requirements

### To develop


#### Install homebrew

1. `xcode-select --install`
2. `curl -L https://raw.githubusercontent.com/Homebrew/install/master/install > ~/brew.rb`
3. `sed -i '' 's/HOMEBREW_PREFIX = .*/HOMEBREW_PREFIX = "#{Dir.home}\/.homebrew"/' ~/brew.rb`
4. `ruby ~/brew.rb`
5. `echo "PATH=$HOME/.homebrew/bin:$PATH" >> ~/.bash_profile`
6. `brew doctor`
7. `rm ~/brew.rb`

#### Setup ruby with rbenv (recommended)

1. `brew install rbenv ruby-build`
2. `echo 'if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi' >> ~/.bash_profile`
3. `source ~/.bash_profile`
4. `rbenv install 2.2.3`
5. `rbenv global 2.2.3`
6. `ruby -v` Should read `Ruby 2.2.3...`

#### Configuring git (recommended)

1. `git config --global color.ui true`
2. `git config --global user.name "Your name"`
3. `git config --global user.email "you@email.com"`
4. `ssh-keygen -t rsa -C "you@email.com`
5. `pbcopy < ~/.ssh/id_rsa.pub`
6. Go to your `bitbucket.org` account.
7. In the top-right corner click on the button and select `Manage Account`.
8. Select `SSH keys` from the sidebar.
9. Select `Add key`.
10. Paste `Cmd + v` (The key is already copied) on the `Key` field. `Label` can be anything.
11. Click `Add key`.

#### Installing Rails (recommended)

1. `gem install rails -v 4.2.4`
2. `rbenv rehash`
3. `rails -v` Should read `Rails 4.2.4`

#### Installing Xcode and cocoapods

1. Install `Xcode` from `App Store`.
2. `sudo gem install cocoapods`
3. `pod setup --verbose`
4. Pull this repo: `https://bitbucket.org/jglievano/mentalmath`
5. `cd mentalmath`
6. `pod install`
7. `open mentalmath.xcworkspace`