import Foundation
import UIKit

// TODO: move to its own .framework.
extension UIButton {

  @IBInspectable var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
      layer.masksToBounds = newValue > 0
    }
  };
    
}
