import UIKit
import GameKit
import Fabric
import Crashlytics
import Flurry_iOS_SDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var gameCenterEnabled: Bool?
  var leaderboardIdentifier: String?

  func application(application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    Fabric.with([Crashlytics.self])
    Flurry.startSession("6KMN7P3GV22G8PVRVVC9")
    Flurry.logEvent("Application Started")
    application.setStatusBarHidden(true, withAnimation: .None)

    MTHGameKitManager.sharedInstance.authenticateLocalPlayer((self.window?.rootViewController)!)
    
    return true
  }

}