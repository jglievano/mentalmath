import UIKit

@IBDesignable
class MTHStylishViewController: UIViewController {
  
  @IBInspectable var showNavigationBar: Bool = true
  @IBInspectable var navBarHeight: CGFloat = 64.0
  @IBInspectable var navBackButtonImage: UIImage = UIImage(named: "backButton")!
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupViews()
  }
  
  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    self.setupViews()
  }
  
  func setupViews() {
    if (self.showNavigationBar) {
      let newNavBar: UINavigationBar =
          UINavigationBar(frame: CGRectMake(0, 0,
                                            CGRectGetWidth(self.view.bounds), self.navBarHeight))
      newNavBar.tintColor = self.navigationController?.navigationBar.tintColor
      newNavBar.barTintColor = self.navigationController?.navigationBar.barTintColor
      let backBarButtonItem: UIBarButtonItem =
          UIBarButtonItem(image: navBackButtonImage,
                          style: .Plain,
                          target: self,
                          action: #selector(MTHStylishViewController.backTapped))
      let newItem: UINavigationItem = UINavigationItem(title: "")
      newItem.leftBarButtonItem = backBarButtonItem
      newNavBar.setItems([newItem], animated: true)
      self.view.addSubview(newNavBar)
    }
  }
  
  func backTapped() {
    self.navigationController?.popViewControllerAnimated(true)
  }
  
}