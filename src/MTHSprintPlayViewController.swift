import UIKit

class MTHSprintPlayViewController: MTHPlayViewController {
  
  let playTrials: Int = 15
  
  override func setupScore() {
    let elapsedTime = self.endTime - self.startTime // Meaning user gets 15 per elapsedTime
    let rate = Double(playTrials) / elapsedTime
    self.score = Int(rate * 60) // Meaning rate is score per minute.
    self.histScore = NSUserDefaults.standardUserDefaults().integerForKey("sprintScore")
    MTHGameKitManager.sharedInstance
        .reportScore(Int64(self.score), leaderboardIdentifier: MTHGameKitManager.kMTHScoreSprint)
    if (self.score > self.histScore) {
      NSUserDefaults.standardUserDefaults().setInteger(self.score, forKey: "sprintScore")
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
  
  override func checkOk() {
    super.checkOk()
    if (self.correctTries >= self.playTrials) {
      self.endPlay()
    }
  }
  
}
