import UIKit

class MTHTimedPlayViewController: MTHPlayViewController {
  
  let PlayTimeout: NSTimeInterval = 60
  
  override func setupScore() {
    self.score = self.correctTries // Meaning rate is score per minute.
    self.histScore = NSUserDefaults.standardUserDefaults().integerForKey("timedScore")
    MTHGameKitManager.sharedInstance
        .reportScore(Int64(self.score), leaderboardIdentifier: MTHGameKitManager.kMTHScoreTimed)
    if (self.score > self.histScore) {
      NSUserDefaults.standardUserDefaults().setInteger(self.score, forKey: "timedScore")
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
  
  override func startPlay() {
    self.playType = PlayType.Timed
    super.startPlay()
    NSTimer.scheduledTimerWithTimeInterval(PlayTimeout,
                                           target: self,
                                           selector: #selector(MTHPlayViewController.endPlay),
                                           userInfo: nil,
                                           repeats: false)
  }
  
}