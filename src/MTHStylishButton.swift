import Foundation
import UIKit

// TODO: move to its own .framework.
@IBDesignable
class MTHStylishButton: UIButton {

  override func awakeFromNib() {
    self.setupViews()
  }
  
  override func prepareForInterfaceBuilder() {
    self.setupViews()
  }
  
  func setupViews() {
  }

}