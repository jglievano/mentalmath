import UIKit

class MTHPlayViewController: MTHStylishViewController {
  
  let bestScoreTitle: String = "You set a new Best Score!"
  let scoreTitle: String = "Your Score:"
  let prevScoreTitle: String = "Previous Best Score:"
  let currScoreTitle: String = "Current Best Score:"
  
  enum PlayType {
    case Timed
    case Sprint
  }
  
  @IBOutlet var actionView: UIView!
  @IBOutlet var result: UILabel!
  @IBOutlet var op1: UILabel!
  @IBOutlet var op2: UILabel!
  @IBOutlet var op: UILabel!
  
  var playType: PlayType = PlayType.Sprint
  
  var startTime: CFAbsoluteTime = 0
  var endTime: CFAbsoluteTime = 0
  
  var correctTries: Int = 0
  var incorrectTries: Int = 0
  
  var score: Int = 0
  var histScore: Int = 0
  
  func setupScore() {
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    startPlay()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if (segue.identifier == "showScore") {
      let next: MTHScoreViewController =
        segue.destinationViewController as! MTHScoreViewController
      setupScore()
      next.scoreValue = String(self.score)
      next.histScoreValue = String(self.histScore)
      if (self.score > self.histScore) {
        next.scoreTitle = self.bestScoreTitle
        next.histScoreTitle = self.prevScoreTitle
      } else {
        next.scoreTitle = self.scoreTitle
        next.histScoreTitle = self.currScoreTitle
      }
    }
  }
  
  func startPlay() {
    self.generateNew()
    self.startTime = CFAbsoluteTimeGetCurrent()
    self.correctTries = 0
    self.incorrectTries = 0
  }
  
  func endPlay() {
    self.endTime = CFAbsoluteTimeGetCurrent()
    self.showScore()
  }
  
  func showScore() {
    self.performSegueWithIdentifier("showScore", sender: nil)
  }
  
  func checkOk() {
    self.correctTries += 1
    self.generateNew()
  }
  
  func checkWrong() {
    self.incorrectTries += 1
    self.result.text = "0"
    UIView.animateWithDuration(0.1, delay: 0, options: .CurveEaseIn, animations: {
      self.actionView.backgroundColor = UIColor.redColor()
      }, completion: {
        (finished: Bool) -> Void in
        UIView.animateWithDuration(0.1, delay: 0, options: .CurveEaseOut, animations: {
          self.actionView.backgroundColor = UIColor.whiteColor()
          }, completion: {
            (finished: Bool) -> Void in
        })
    })
  }
  
  func generateNew() {
    var option: Int = Int(arc4random_uniform(100))
    if (option > 50) {
      self.op.text = "+"
    } else {
      self.op.text = "-"
    }
    option = Int(arc4random_uniform(100))
    while (option <= 0) {
      option = Int(arc4random_uniform(100))
    }
    self.op1.text = String(option)
    option = Int(arc4random_uniform(100))
    if (self.op.text! == "-") {
      while (option > Int(self.op1.text!)! || option <= 0) {
        option = Int(arc4random_uniform(100))
      }
    }
    self.op2.text = String(option)
    self.result.text = "0"
  }
  
  func processAnswer() {
    var answer: Int = 0
    switch (op.text!) {
    case "+":
      answer = Int(self.op1.text!)! + Int(self.op2.text!)!
    case "-":
      answer = Int(self.op1.text!)! - Int(self.op2.text!)!
    default:
      answer = 0
    }
    if (Int(self.result.text!)! == answer) {
      checkOk()
    } else {
      checkWrong()
    }
  }
  
  func processDigit(digit: Int) {
    if (Int(self.result.text!) == 0) {
      self.result.text = String(digit)
    } else {
      self.result.text = self.result.text! + String(digit)
    }
  }
  
  func removeDigit() {
    var currentResult: String = self.result.text!
    if (currentResult.characters.count > 1) {
      currentResult.removeAtIndex(currentResult.endIndex.predecessor())
      self.result.text = currentResult
    } else {
      self.result.text = "0"
    }
  }
  
  @IBAction func tapNumpad0(sender: AnyObject) {
    processDigit(0)
  }
  
  @IBAction func tapNumpad1(sender: AnyObject) {
    processDigit(1)
  }
  
  @IBAction func tapNumpad2(sender: AnyObject) {
    processDigit(2)
  }
  
  @IBAction func tapNumpad3(sender: AnyObject) {
    processDigit(3)
  }
  
  @IBAction func tapNumpad4(sender: AnyObject) {
    processDigit(4)
  }
  
  @IBAction func tapNumpad5(sender: AnyObject) {
    processDigit(5)
  }
  
  @IBAction func tapNumpad6(sender: AnyObject) {
    processDigit(6)
  }
  
  @IBAction func tapNumpad7(sender: AnyObject) {
    processDigit(7)
  }
  
  @IBAction func tapNumpad8(sender: AnyObject) {
    processDigit(8)
  }
  
  @IBAction func tapNumpad9(sender: AnyObject) {
    processDigit(9)
  }
  
  @IBAction func tapNumpadBack(sender: AnyObject) {
    removeDigit()
  }
  
  @IBAction func tapNumpadOK(sender: AnyObject) {
    processAnswer()
  }
  
}