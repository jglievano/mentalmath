import UIKit

class MTHScoreViewController: MTHStylishViewController {
  
  @IBOutlet var scoreTitleLabel: UILabel!
  @IBOutlet var scoreValueLabel: UILabel!
  @IBOutlet var histScoreTitleLabel: UILabel!
  @IBOutlet var histScoreValueLabel: UILabel!
  
  var scoreTitle: String = ""
  var scoreValue: String = ""
  var histScoreTitle: String = ""
  var histScoreValue: String = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    scoreTitleLabel.text = scoreTitle
    scoreValueLabel.text = scoreValue
    histScoreTitleLabel.text = histScoreTitle
    histScoreValueLabel.text = histScoreValue
  }
  
}
