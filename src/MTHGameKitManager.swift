import Foundation
import GameKit

class MTHGameKitManager: NSObject, UINavigationControllerDelegate,
    GKGameCenterControllerDelegate {

  static let kMTHScoreTimed: String = "mathpunch_timed_best"
  static let kMTHScoreSprint: String = "mathpunch_sprint_best"

  var rootViewController: UIViewController?
  var gameCenterEnabled: Bool?

  class var sharedInstance: MTHGameKitManager {
    struct Singleton {
      static let instance = MTHGameKitManager()
    }
    return Singleton.instance
  }

  // leaderboardIdentifier can be either:
  // 1. matchpunch_timed_best (kMTHScoreTimed)
  // 2. matchpunch_sprint_best (kMTHScoreSprint)
  func reportScore(scoreValue: Int64, leaderboardIdentifier: String) {
    let score: GKScore = GKScore.init(leaderboardIdentifier: leaderboardIdentifier)
    score.value = scoreValue

    GKScore.reportScores([score], withCompletionHandler: {(error: NSError?) -> Void in
      if (error != nil) {
        NSLog("\(error?.localizedDescription)")
      }
    })
  }
  
  func authenticateLocalPlayer(rootViewController: UIViewController) {
    self.rootViewController = rootViewController
    let localPlayer:GKLocalPlayer = GKLocalPlayer.localPlayer()
    localPlayer.authenticateHandler =
        {(viewController: UIViewController?, error: NSError?) -> Void in
          if (viewController != nil) {
            self.rootViewController?
                .presentViewController(viewController!, animated: true, completion: nil)
          } else {
            if (GKLocalPlayer.localPlayer().authenticated) {
              self.gameCenterEnabled = true;
            } else {
              self.gameCenterEnabled = false;
            }
          }
        }
  }

  func showLeaderboard(leaderboardIdentifier: String) {
    let gcViewController: GKGameCenterViewController = GKGameCenterViewController()
    gcViewController.delegate = self
    gcViewController.viewState = .Leaderboards
    self.rootViewController?.presentViewController(gcViewController,
                                                   animated: true,
                                                   completion: nil)
  }

  func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
    [gameCenterViewController .dismissViewControllerAnimated(true, completion: nil)]
  }

}